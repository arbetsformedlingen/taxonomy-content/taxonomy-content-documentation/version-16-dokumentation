# Version 16 Dokumentation

Här samlas information om version 16 av Taxonomin. 

Om det finns behov av mer material eller information så är det bara att höra av sig. 

Version 16 är enbart till för system inom Arbetsförmedlingen som idag använder sig av version 1. Versionen har tagits fram för att förenkla inhämtandet av yrkesbenämningar som skapats sedan version 1. 

_Förändringar i yrkesregistret i version 16 avser enbart tillägg av yrkesbenämningar_.

För att använda version 16 behöver versionsparametern specifceras med "16" i queries till API:et.

## Definitioner

### Yrkesregistret

Med "yrkesregistret" avses i den här texten begrepp av typerna `occupation-name`, `ssyk-level-4` och `occupation-field`, och relationerna däremellan. 

### Begreppsförtydliganden

Se https://confluence.arbetsformedlingen.se/pages/viewpage.action?pageId=249070446

## Version 16

För att skapa version 16 har tillvägagångssättet varit att utgå från version 15 och "spola tillbaka" tiden för begrepp och relationer inom yrkesregistret för att de ska se ut som de gjorde i version 1, samtidigt som helt nya yrkesbenämningar, eller `occupation-name`, har behållits.

För yrkesområden, eller `occupation-field`, och SSYK nivå 4-grupper, eller `ssyk-level-4`, finns inga nya begrepp. 

Samtidigt som version 16 skapades så skapades även version 17, där förändringarna i version 16 återtogs.  

### Detaljerad information

Tillbakaspolning har utförts på följande förändringar:

- namnändringar (~~för `ssyk-level-4`~~,`occupation-name` och `occupation-field`)
  - OBS att `ssyk-level-4` *egentligen* inte ska ändra namn, men i det här fallet har ändringarna avsett felstavningar och utskrivning av förkortningar. Dessa har också spolats tillbaka
  - OBS2 Det har visat sig att `ssyk-level-4`-begrepp faktiskt har ändrat `preferred-label` i version 16
- relationsförändringar (för `ssyk-level-4`, `occupation-name` och `occupation-field`)
  - gäller occupation-name-begrepp som "bytt SSYK nivå 4-grupp", dvs fått annan relation till en ssyk-level-4-grupp
  - gäller ssyk-level-4-grupper som bytt yrkesområde, dvs fått relation till ett annat occupation-field
- deprecated-status (för `occupation-name`)
  - samtliga occupation-name-begrepp som har `deprecated: false`i version 1 har det också i version 16.
  - inga yrkesområden eller SSYK nivå 4-grupper har blivit deprecated sedan version 1

## Avgränsningar

- Det är enbart yrkesregistret som berörs av tillbakaspolningen av Taxonomin. Andra begrepp, som kompetensbegrepp, keyword och esco-yrken har inte påverkats.
- Inga yrkesbenämningar som *tillkommit och blivit deprecated* mellan version 1 och 16 är inkluderade. 

## Exempel

Länk graphqiql: https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql

### Yrkesregister version 16

```graphql
query MyQuery {
  concepts(version: "16", type: "occupation-field") {
    id
    preferred_label
    type
    narrower(type: "ssyk-level-4") {
      id
      preferred_label
      ssyk_code_2012
      narrower(type: "occupation-name") {
        id
        preferred_label
        type
      }
    }
  }
}
```

### Angående occupation-name-begrepp som är deprecated i version 16

Observera att vissa occupation-name-begrepp kommer att vara deprecated i version 16 trots att versionen bara ska innehålla tillägg. Det har att göra med att begrepp kan ha lagts till under perioden version 2 till och med version 15 och sedan blivit deprecated under samma period. Dessa begrepp "har aldrig funnits" sett till uppgraderingen version 1 -> version 16. Se exempelvis följande query och begreppet "Språkstödjare" :

```graphql
query MyQuery {
  concepts(include_deprecated: true, version: "16", id: "6gi1_3Ss_E8c") {
    id
    preferred_label
    type
    deprecated  
  }
}
```

## Kända problem

Ibland har begrepp satts till deprecated för att hänvisa till andra begrepp som är näraliggande men inte exakt samma. Eftersom att dessa begrepp i version 16 inte är deprecated kommer det innebära att det kommer finnas ett antal begrepp som ser väldigt lika ut för användarna. Se exempelvis "Lärare i praktiska och estetiska ämnen"  (nedan i version 15): 


```json
{
  "id": "ajWc_95L_PnQ",
  "preferred_label": "Lärare i praktiska och estetiska ämnen",
  "type": "occupation-name",
  "deprecated": true,
  "replaced_by": [
      {
        "id": "86sy_hBf_exW",
        "preferred_label": "Ämneslärare, gymnasieskolan"
    }
  ]
}
```

I version 16 finns både "Lärare i praktiska och estetiska ämnen" och "Ämneslärare, gymnasieskolan".

## Filer

### v1_v16_nya_yrken.xlsx

Yrkesbenämningar som tillkommit från och med version 1 till och med version 16 *och* som inte är deprecated finns här

### yrkesbenämningar_som_blir_deprecated_med_hänvisning.xlsx

Yrkesbenämningar som _egentligen_ blivit deprecated och fått hänvisning till annan yrkesbenämning från och med version 1 till och med version 16 listas här. OBS att dessa yrkesbenämningar inte är deprecated och hänvisade i version 16 (i och med att version 16 inte beaktar dessa förändringar). Listan kan vara av intresse i supportsammanhang. 

### v16_alla_yrkesbenämningar.xlsx

Samtliga yrkesbenämningar (ej `deprecated`) i version 16.
